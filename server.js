var fs = require('fs');
var http = require('http');
var url = require('url');

http.createServer((req, res) => {
  var path = url.parse(req.url).pathname.split("/");
  switch (path[1]) {
    case "dynamic":
      break;
    case "static":
      fs.readFile(["staticContent"].concat(path.slice(2)).join("/"), (err, data) => {
        res.write(data);
        res.end();
      });
      break;
  }
}).listen(80);
